<?php

namespace Drupal\modular_finance;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Modular finance type entities.
 */
class ModularFinanceTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Modular finance type');
    $header['id'] = $this->t('Machine name');
    $header['widget_type'] = $this->t('Widget type');
    $header['widget_token'] = $this->t('Widget token');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['widget_type'] = $entity->getWidgetType();
    $row['widget_token'] = $entity->getWidgetToken();
    return $row + parent::buildRow($entity);
  }

}
