<?php

namespace Drupal\modular_finance\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ModularFinanceBlock' block.
 *
 * @Block(
 *  id = "modular_finance_block",
 *  admin_label = @Translation("Modular finance block"),
 * )
 */
class ModularFinanceBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * The kml user manager.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $modular_finance_types = $this->entityTypeManager
      ->getStorage('modular_finance_type')
      ->loadMultiple();
    $options = [];
    /** @var \Drupal\modular_finance\Entity\ModularFinanceType $modular_finance_type */
    foreach ($modular_finance_types as $modular_finance_type) {
      $options[$modular_finance_type->id()] = $modular_finance_type->label();
    }
    $form['modular_finance_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Block type'),
      '#description' => $this->t('Select the block type'),
      '#options' => $options,
      '#default_value' => $this->configuration['modular_finance_type'] ?? NULL,
      '#weight' => '0',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['modular_finance_type'] = $form_state->getValue('modular_finance_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\modular_finance\Entity\ModularFinanceType $modular_finance_type */
    $modular_finance_type = $this->entityTypeManager
      ->getStorage('modular_finance_type')
      ->load($this->configuration['modular_finance_type']);
    // In order to be able to place different blocks in the same page we need to
    // add an identifier to the drupalSettings.
    $settings['modularFinance'][$modular_finance_type->getWidgetToken()] = [
      'query' => '[data-token="' . $modular_finance_type->getWidgetToken() . '"]',
      'widget' => $modular_finance_type->getWidgetType(),
      'token' => $modular_finance_type->getWidgetToken(),
      'locale' => $this->languageManager->getCurrentLanguage()->getId(),
      'c' => $this->configFactory->get('modular_finance.settings')->get('client_token'),
    ];
    return [
      'modular_finance_element' => [
        '#type' => 'container',
        '#attributes' => [
          'data-token' => $modular_finance_type->getWidgetToken(),
          'class' => [$modular_finance_type->id()],
        ],
        '#attached' => [
          'library' => 'modular_finance/modular-finance',
          'drupalSettings' => $settings,
        ],
      ],
    ];
  }

}
