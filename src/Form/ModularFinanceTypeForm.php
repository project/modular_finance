<?php

namespace Drupal\modular_finance\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ModularFinanceTypeForm.
 */
class ModularFinanceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $modular_finance_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $modular_finance_type->label(),
      '#description' => $this->t('Label for the Modular finance type.'),
      '#required' => TRUE,
    ];

    $form['widget_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget type'),
      '#maxlength' => 255,
      '#default_value' => $modular_finance_type->getWidgetType(),
      '#description' => $this->t('Widget type for the Modular finance type.'),
      '#required' => TRUE,
    ];

    $form['widget_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget token'),
      '#maxlength' => 255,
      '#default_value' => $modular_finance_type->getWidgetToken(),
      '#description' => $this->t('Widget token for the Modular finance type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $modular_finance_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\modular_finance\Entity\ModularFinanceType::load',
      ],
      '#disabled' => !$modular_finance_type->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $modular_finance_type = $this->entity;
    $status = $modular_finance_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Modular finance type.', [
          '%label' => $modular_finance_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Modular finance type.', [
          '%label' => $modular_finance_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($modular_finance_type->toUrl('collection'));
  }

}
