<?php

namespace Drupal\modular_finance\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ModularFinanceSettingsForm.
 */
class ModularFinanceSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'modular_finance.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modular_finance_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('modular_finance.settings');
    $form['client_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client token'),
      '#description' => $this->t('Insert the client token'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('client_token'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('modular_finance.settings')
      ->set('tokens', $form_state->getValue('tokens'))
      ->set('client_token', $form_state->getValue('client_token'))
      ->save();
  }

}
