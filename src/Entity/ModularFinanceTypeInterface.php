<?php

namespace Drupal\modular_finance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Modular finance type entities.
 */
interface ModularFinanceTypeInterface extends ConfigEntityInterface {

  /**
   * Get the widget type.
   *
   * @return string
   *   The widget type.
   */
  public function getWidgetType();

  /**
   * Get the widget token.
   *
   * @return string
   *   The widget token.
   */
  public function getWidgetToken();

}
