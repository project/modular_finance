<?php

namespace Drupal\modular_finance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Modular finance type entity.
 *
 * @ConfigEntityType(
 *   id = "modular_finance_type",
 *   label = @Translation("Modular finance type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\modular_finance\ModularFinanceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\modular_finance\Form\ModularFinanceTypeForm",
 *       "edit" = "Drupal\modular_finance\Form\ModularFinanceTypeForm",
 *       "delete" = "Drupal\modular_finance\Form\ModularFinanceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\modular_finance\ModularFinanceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "modular_finance_type",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/modular_finance_type/{modular_finance_type}",
 *     "add-form" = "/admin/structure/modular_finance_type/add",
 *     "edit-form" = "/admin/structure/modular_finance_type/{modular_finance_type}/edit",
 *     "delete-form" = "/admin/structure/modular_finance_type/{modular_finance_type}/delete",
 *     "collection" = "/admin/structure/modular_finance_type"
 *   }
 * )
 */
class ModularFinanceType extends ConfigEntityBase implements ModularFinanceTypeInterface {

  /**
   * The Modular finance type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Modular finance type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Modular finance type token.
   *
   * @var string
   */
  protected $widget_token;

  /**
   * The Modular finance type type.
   *
   * @var string
   */
  protected $widget_type;

  /**
   * {@inheritDoc}
   */
  public function getWidgetToken() {
    return $this->widget_token;
  }

  /**
   * {@inheritDoc}
   */
  public function getWidgetType() {
    return $this->widget_type;
  }

}
