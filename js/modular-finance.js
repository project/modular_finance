(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.initModularFinance = {
    attach: function (context) {
      let modularFinanceData = [];
      // Reset the array to be understood by the script.
      for (let i in drupalSettings.modularFinance) {
        modularFinanceData.push(drupalSettings.modularFinance[i]);
      }
      window._MF = {
        data: modularFinanceData,
        url: "https://widget.datablocks.se/api/rose",
        ready: !!0,
        render: function () {
          window._MF.ready = !0
        }
      };
      var b = document.createElement("script");
      b.type = 'text/javascript';
      b.async = true;
      b.src = window._MF.url + "/assets/js/loader-v2.js";
      document.getElementsByTagName("body")[0].appendChild(b)
    }
  };
})(jQuery, Drupal, drupalSettings);
